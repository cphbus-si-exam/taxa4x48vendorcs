namespace taxa
{
    public class TransportResModel
    {
        public string eventid { get; set; }
        public string description { get; set; }
        public bool status { get; set; }
        public string additional_info { get; set; }   
        public string request_type { get; set; }    
    }
}
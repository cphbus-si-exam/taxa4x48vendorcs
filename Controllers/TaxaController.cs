using System.Net;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace taxa
{
    [ApiController]
    [Route("[controller]")]
    public class TaxaController : Controller
    {

        [HttpGet]
        public IActionResult Get(){
            return Ok("Taxa vendor is alive");
        }

        [HttpPost]
        public IActionResult Post([FromBody] TransportModel[] transreq){
            var resmodel = new TransportResModel();
            resmodel.eventid = transreq[0].eventid;
            resmodel.request_type = transreq[0].request_type;
            resmodel.status=true;
            resmodel.additional_info = "Contact person: Perniller Hoyer";
            
            return Json(resmodel);
            
        }


    }
}